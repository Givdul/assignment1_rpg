# Assignment 1: RPG Characters

Assignent 1 RPG allows you to make a character of four different classes: Ranger, Rogue, Mage and Warrior. Your character can level up and equip some gear and a weapon. 

## Usage

```c
# create new mage character
Mage testMage = new Mage();

# create new weapon item
Weapon testStaff = new Weapon();

# updates characters stats
Character.Stats(Level);

# calculates characters damage
Character.DPS(Weapon weapon);

# equips an item and checks if the character can equip it
Character.equipItem(weapon)

# prints all stats to strings which you can write to the console
testMage.printStats((Weapon)testMage.WeaponSlot));
```
## Author
Ludvig Hansen. 

## License
[MIT](https://choosealicense.com/licenses/mit/)