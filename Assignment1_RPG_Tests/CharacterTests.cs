using Assignment1_RPG;
using System;
using Xunit;

namespace Assignment1_RPG_Tests
{
    public class CharacterTests
    {
        [Fact]
        public void WhenCharacterIsCreated_LevelIs1()
        {
            //Arrange
            Mage testObj = new Mage()
            {
                Name = "Test Character",
            };
            int expected = 1;
            //Act
            int actual = testObj.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WhenCharacterLevelsUp_AddsOneLevel()
        {
            //Arrange
            Mage testObj = new Mage()
            {
                Name = "Test Character",
            };
            testObj.levelUp();
            int expected = 2;
            //Act
            int actual = testObj.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MageBaseStats_IsCorrect()
        {
            //Arrange
            Mage testObj2 = new Mage()
            {
                Name = "Test Character",
            };
            int expectedStrength = 1;
            int expectedDexterity = 1;
            int expectedIntelligence = 8;
            //Act
            int actualStrength = testObj2.Strength;
            int actualDexterity = testObj2.Dexterity;
            int actualIntelligence = testObj2.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void RangerBaseStats_IsCorrect()
        {
            //Arrange
            Ranger testObj = new Ranger()
            {
                Name = "Test Character",
            };
            int expectedStrength = 1;
            int expectedDexterity = 7;
            int expectedIntelligence = 1;
            //Act
            int actualStrength = testObj.Strength;
            int actualDexterity = testObj.Dexterity;
            int actualIntelligence = testObj.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void RogueBaseStats_IsCorrect()
        {
            //Arrange
            Rogue testObj = new Rogue()
            {
                Name = "Test Character",
            };
            int expectedStrength = 2;
            int expectedDexterity = 6;
            int expectedIntelligence = 1;
            //Act
            int actualStrength = testObj.Strength;
            int actualDexterity = testObj.Dexterity;
            int actualIntelligence = testObj.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void WarriorBaseStats_IsCorrect()
        {
            //Arrange
            Warrior testObj = new Warrior()
            {
                Name = "Test Character",
            };
            int expectedStrength = 5;
            int expectedDexterity = 2;
            int expectedIntelligence = 1;
            //Act
            int actualStrength = testObj.Strength;
            int actualDexterity = testObj.Dexterity;
            int actualIntelligence = testObj.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void MageStats_OnLevelUp_IsCorrect()
        {
            //Arrange
            Mage testObj = new Mage()
            {
                Name = "Test Character",
            };
            testObj.levelUp();
            testObj.Stats(testObj.Level);
            int expectedStrength = 1 + 1;
            int expectedDexterity = 1 + 1;
            int expectedIntelligence = 8 + 5;
            //Act
            int actualStrength = testObj.Strength;
            int actualDexterity = testObj.Dexterity;
            int actualIntelligence = testObj.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void RangerStats_OnLevelUp_IsCorrect()
        {
            //Arrange
            Ranger testObj = new Ranger()
            {
                Name = "Test Character",
            };
            testObj.levelUp();
            testObj.Stats(testObj.Level);
            int expectedStrength = 1 + 1;
            int expectedDexterity = 7 + 5;
            int expectedIntelligence = 1 + 1;
            //Act
            int actualStrength = testObj.Strength;
            int actualDexterity = testObj.Dexterity;
            int actualIntelligence = testObj.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void RogueStats_OnLevelUp_IsCorrect()
        {
            //Arrange
            Rogue testObj = new Rogue()
            {
                Name = "Test Character",
            };
            testObj.levelUp();
            testObj.Stats(testObj.Level);
            int expectedStrength = 2 + 1;
            int expectedDexterity = 6 + 4;
            int expectedIntelligence = 1 + 1;
            //Act
            int actualStrength = testObj.Strength;
            int actualDexterity = testObj.Dexterity;
            int actualIntelligence = testObj.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void WarriorStats_OnLevelUp_IsCorrect()
        {
            //Arrange
            Warrior testObj = new Warrior()
            {
                Name = "Test Character",
            };
            testObj.levelUp();
            testObj.Stats(testObj.Level);
            int expectedStrength = 5 + 3;
            int expectedDexterity = 2 + 2;
            int expectedIntelligence = 1 + 1;
            //Act
            int actualStrength = testObj.Strength;
            int actualDexterity = testObj.Dexterity;
            int actualIntelligence = testObj.Intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }
    }
}
