﻿using Assignment1_RPG;
using Assignment1_RPG.CustomExceptions;
using Assignment1_RPG.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Assignment1_RPG_Tests
{
    public class ItemTests
    {
        
        [Fact]
        public void CharacterEquipsHighItemLevel_SHouldTrowItemLevelException()
        {
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                ItemLevel = 2,
                ItemType = "Axe",
                Damage = 7,
                AttackSpeed = 1.1,
            };
            Assert.Throws<InvalidItemLevelException>(() => testWarrior.equipItem(testAxe));
        }

        [Fact]
        public void CharacterEquipsWrongWeaponType_SHouldTrowWeaponException()
        {
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Weapon testBow = new Weapon()
            {
                Name = "Common Bow",
                ItemLevel = 1,
                ItemType = "Bow",
                Damage = 12,
                AttackSpeed = 0.8,
            };
            Assert.Throws<InvalidWeaponException>(() => testWarrior.equipItem(testBow));
        }

        [Fact]
        public void CharacterEquipsWrongArmourType_SHouldTrowArmorException()
        {
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Armor testClothHead = new Armor()
            {
                Name = "Common cloth head armor",
                ItemLevel = 1,
                ItemType = "Cloth",
                Intelligence = 5,
            };
            Assert.Throws<InvalidArmorException>(() => testWarrior.equipItem(testClothHead));
        }

        [Fact]
        public void CharacterEquipsWeaponSucessfully_RecieveMessage()
        {
            //Arrange
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                ItemLevel = 1,
                ItemType = "Axe",
                Damage = 7,
                AttackSpeed = 1.1,
            };
            string expected = "New weapon equipped!";
            //Act
            string actual = testWarrior.equipItem(testAxe);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterEquipsArmourSucessfully_RecieveMessage()
        {
            //Arrange
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Body testBody = new Body()
            {
                Name = "Common plate body armour",
                ItemLevel = 1,
                ItemType = "Plate",
            };
            string expected = "New armour equipped!";
            //Act
            string actual = testWarrior.equipItem(testBody);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterDamageWithoutWeapon_IsCorrect()
        {
            //Arrange
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            testWarrior.DPS(testWarrior.WeaponSlot);
            double expected = 1.0 * (1.0 + (5.0 / 100.0));
            //Act
            double actual = testWarrior.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterWithWeapon_DamageCalculated()
        {
            //Arrange
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                ItemLevel = 1,
                ItemType = "Axe",
                Damage = 7,
                AttackSpeed = 1.1,
            };
            testAxe.DPS = testAxe.WeaponDPS(testAxe.Damage, testAxe.AttackSpeed);
            testWarrior.equipItem(testAxe);
            testWarrior.DPS(testWarrior.WeaponSlot);
            double expected = (7.0 * 1.1) *(1.0 + (5.0 / 100.0));
            //Act
            double actual = testWarrior.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterWithWeaponAndArmor_DamageCalculated()
        {
            //Arrange
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                ItemLevel = 1,
                ItemType = "Axe",
                Damage = 7,
                AttackSpeed = 1.1,
            };
            testAxe.DPS = testAxe.WeaponDPS(testAxe.Damage, testAxe.AttackSpeed);
            testWarrior.equipItem(testAxe);
            Body testBody = new Body()
            {
                Name = "Common plate body armour",
                ItemLevel = 1,
                ItemType = "Plate",
                Strength = 1,
            };
            testWarrior.equipItem(testBody);
            testWarrior.DPS(testWarrior.WeaponSlot);
            double expected = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0));
            //Act
            double actual = (double)testWarrior.Damage;
            //Arrange
            Assert.Equal(expected , actual);
        }
    }
}
