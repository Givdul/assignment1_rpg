﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPG
{
    public abstract class Item
    {
        public string Name { get; set; }
        public string ItemType { get; set; }
        public int ItemLevel { get; set; }
        /// <summary>
        /// Calculates the Weapons damage per second from the Damage and Attackspeed it has.
        /// </summary>
        /// <param name="Damage">The Weapons Damage</param>
        /// <param name="AttackSpeed">The Weapons Attack Speed</param>
        /// <returns>Damage * AttackSpeed</returns>
        public double WeaponDPS(double Damage, double AttackSpeed)
        {
            return Damage * AttackSpeed;
        }
    }

    

}
