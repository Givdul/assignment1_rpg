﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPG
{
    public class Weapon : Item
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get; set; }
    }
}
