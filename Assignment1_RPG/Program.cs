﻿using Assignment1_RPG.Items;
using System;

namespace Assignment1_RPG
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Creating the testWarrior from the Item testing so we can print his stats to the console
            Warrior testWarrior = new Warrior()
            {
                Name = "Test Warrior",
            };
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                ItemLevel = 1,
                ItemType = "Axe",
                Damage = 7,
                AttackSpeed = 1.1,
            };
            testWarrior.levelUp();
            testAxe.DPS = testAxe.WeaponDPS(testAxe.Damage, testAxe.AttackSpeed);
            testWarrior.equipItem(testAxe);
            Body testBody = new Body()
            {
                Name = "Common plate body armour",
                ItemLevel = 1,
                ItemType = "Plate",
                Strength = 1,
            };
            testWarrior.equipItem(testBody);
            testWarrior.DPS(testWarrior.WeaponSlot);
            //Print stats to console Requires our weapon as a paramater
            Console.WriteLine(testWarrior.printStats((Weapon)testWarrior.WeaponSlot));

            //Creating a mage object, a weapon object and three armor objects
            Weapon testStaff = new Weapon()
            {
                Name = "Staff of armadyl",
                ItemType = "Staff",
                ItemLevel = 9,
            };
            testStaff.DPS = (testStaff.WeaponDPS(testStaff.Damage = 20, testStaff.AttackSpeed = 0.8));
            Head testArmor = new Head()
            {
                Name = "Ancestral hat",
                ItemType = "Cloth",
                Strength = 14,
                Dexterity = 28,
                Intelligence =  38,
                ItemLevel = 30,
            };

            Body testArmor2 = new Body()
            {
                Name = "Ancestral robe top",
                ItemType = "Cloth",
                Strength = 14,
                Dexterity = 10,
                Intelligence = 63,
                ItemLevel = 30,
            };

            Legs testArmor3 = new Legs()
            {
                Name = "Ancestral robe legs",
                ItemType = "Cloth",
                Strength = 27,
                Dexterity = 24,
                Intelligence = 46,
                ItemLevel = 30,
            };

            Mage mageObj = new Mage()
            {
                Name = "Zezima",
                Level = 126,
            };
            //Equip all Items
            mageObj.equipItem(testStaff);
            mageObj.equipItem(testArmor);
            mageObj.equipItem(testArmor2);
            mageObj.equipItem(testArmor3);
            //Print stats to console Requires our weapon as a paramater
            Console.WriteLine(mageObj.printStats((Weapon)mageObj.WeaponSlot));      
           
        }
    }
}
