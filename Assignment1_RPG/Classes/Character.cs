﻿using Assignment1_RPG.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPG
{
    /// <summary>
    /// Character is the Parent class of the Four Classes in this RPG.
    /// Child classes are: Mage.cs, Warrior.cs, Ranger.cs and Rogue.cs
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public double PrimaryAttribute { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public double Damage {get; set; }
        public Weapon WeaponSlot { get; set; }
        public Armor Head { get; set; }
        public Armor Body { get; set; }
        public Armor Legs { get; set; }

        public Character()
        {
            Level = 1;
        }

        /// <summary>
        /// Sets the base stat for a character and updates it based on their level.
        /// Also Sets the correct PrimaryAttribute for the type of character.
        /// </summary>
        /// <param name="lvl">Level of Character</param>
        public abstract void Stats(int lvl);

        /// <summary>
        /// Invokes the Stats method to update all stats.
        /// Checks if the Characters Weapon slot has a Weapon stored.
        /// If it does not have a weapon it calculates 1% of damage per point of primary attribute * 1.
        /// If it has a weapon it Calculates the weapons DPS * 1% of damage per point of primary attribute.
        /// </summary>
        /// <param name="weapon">Weapon object</param>
        public void DPS(Weapon weapon)
        {
            
            Stats(Level);
            if (weapon == null)
                Damage = 1 * 1 + (PrimaryAttribute/100);
            else
            {
                Damage = weapon.DPS * ( 1 + (PrimaryAttribute / 100));
            }

               
        }
        /// <summary>
        /// Checks if a character can equip an item.
        /// </summary>
        /// <param name="item">Item object</param>
        /// <param name="lvl">Character Level</param>
        /// <returns>Bool or Exception handler</returns>
        /// <exception cref="InvalidItemLevelException">Exception is thrown if a Character is lower Level than the Item requires</exception>
        public abstract bool ItemCheck(Item item, int lvl);

        /// <summary>
        /// Uses ItemCheck method to check if an item can be equipped by the character
        /// If the check goes trough the item is stored.
        /// If the check returns false it checks for what type of item it was and throws the correct exceptionhandler.
        /// </summary>
        /// <param name="toBeEquipped">Item Object</param>
        /// <returns>Item Object</returns>
        /// <exception cref="InvalidWeaponException">Exception is thrown if Character cannot equip this weapon type</exception>
        /// <exception cref="InvalidArmorException">Exception is thrownCharacter cannot equip this armor type</exception>
        public string equipItem(Item toBeEquipped)
        {
            string Type = toBeEquipped.GetType().ToString();
            if (ItemCheck(toBeEquipped, Level))
            {
                if (Type == "Assignment1_RPG.Weapon")
                {
                    WeaponSlot = (Weapon)toBeEquipped;
                    return "New weapon equipped!";
                }
                if (Type == "Assignment1_RPG.Items.Head")
                {
                    Head = (Armor)toBeEquipped;
                    return "New armour equipped!";
                }
                if (Type == "Assignment1_RPG.Items.Body")
                {
                    Body = (Armor)toBeEquipped;
                    return "New armour equipped!";
                }
                if (Type == "Assignment1_RPG.Items.Legs")
                {
                    Legs = (Armor)toBeEquipped;
                    return "New armour equipped!";
                }
            }

            else
            {
                if (Type == "Assignment1_RPG.Weapon")
                {
                    throw new InvalidWeaponException("Cannot equip this type of Weapon");
                }
            }
            throw new InvalidArmorException("Cannot equip this type of Armor");

        }
        /// <summary>
        /// Adds the total amount of a characters primaryattribute from their base stats times level  and any Armor items equipped.
        /// </summary>
        /// <returns>Total amount of TotalAttribute in an integer</returns>
        public void totalPrimaryAttribute()
        {
            if (Head != null)
            {
                Strength = Strength + Head.Strength;
                Dexterity = Dexterity + Head.Dexterity;
                Intelligence = Intelligence + Head.Intelligence;
            }

            if (Body != null)
            {
                Strength = Strength + Body.Strength;
                Dexterity = Dexterity + Body.Dexterity;
                Intelligence = Intelligence + Body.Intelligence;
            }


            if (Legs != null)
            {
                Strength = Strength + Legs.Strength;
                Dexterity = Dexterity + Legs.Dexterity;
                Intelligence = Intelligence + Legs.Intelligence;
            }
        }

        /// <summary>
        /// Adds one level to the character and updates its stats.
        /// </summary>
        public void levelUp()
        {
            Stats(Level++);
        }
        /// <summary>
        /// Updates and Prints all the current stats and information about our character.
        /// Uses the DPS method to update stats.
        /// </summary>
        /// <param name="Weapon">Sends weapon to calculate damage with weapon DPS</param>
        /// <returns>String built from all information and stats our character has</returns>
        public string printStats(Weapon Weapon)
        {
            DPS(Weapon);
            StringBuilder print = new StringBuilder();
            print.AppendLine("Name: " + Name);
            print.AppendLine("Level: " + Level);
            print.AppendLine("Strength: " + Strength);
            print.AppendLine("Dexterity: " + Dexterity);
            print.AppendLine("Intelligence: " + Intelligence);
            print.AppendLine("Damage: " + Damage);
            if (WeaponSlot != null)
                print.AppendLine("Weapon: " + WeaponSlot.Name);

            if (Head != null)
            print.AppendLine("Head: " + Head.Name);

            if (Body != null)
                print.AppendLine("Body: " + Body.Name);

            if (Legs != null)
                print.AppendLine("Legs: " + Legs.Name);

            return print.ToString();
        }
    }
}
