﻿using Assignment1_RPG.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPG
{
    public class Warrior : Character
    {
        public Warrior()
        {
            Strength = 5;
            Dexterity = 2;
            Intelligence = 1;
            PrimaryAttribute = Strength;
        }
        public override void Stats(int lvl)
        {
            lvl = lvl - 1;
            Strength = Strength + (lvl * 3);
            Dexterity = Dexterity + (lvl * 2);
            Intelligence = Intelligence + (lvl * 1);
            totalPrimaryAttribute();
            PrimaryAttribute = Strength;
        }

        public override bool ItemCheck(Item item, int Level)
        {
            string[] allowedItems = new string[] { "Plate", "Mail", "Axe", "Hammer", "Sword" };
            if (item.ItemLevel <= Level)
            {
                foreach (string allowedItem in allowedItems)
                {
                    if (allowedItem == item.ItemType)
                        return true;
                };
            }
            if (item.ItemLevel > Level)
            {
                throw new InvalidItemLevelException("You are not high enough level to equip this item");
            }
            return false;
        }
    }
}
