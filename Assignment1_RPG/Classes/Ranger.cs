﻿using Assignment1_RPG.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPG
{
    public class Ranger : Character
    {
        public Ranger()
        {
            Strength = 1;
            Dexterity = 7;
            Intelligence = 1;
            PrimaryAttribute = Dexterity;
        }
        public override void Stats(int lvl)
        {
            lvl = lvl - 1;
            Strength = Strength + (lvl * 1);
            Dexterity = Dexterity + (lvl * 5);
            Intelligence = Intelligence + (lvl * 1);
            totalPrimaryAttribute();
            PrimaryAttribute = Dexterity;
        }

        public override bool ItemCheck(Item item, int Level)
        {
            string[] allowedItems = new string[] { "Leather", "Mail", "Bow" };
            if (item.ItemLevel <= Level)
            {
                foreach (string allowedItem in allowedItems)
                {
                    if (allowedItem == item.ItemType)
                        return true;
                };
            }
            if (item.ItemLevel > Level)
            {
                throw new InvalidItemLevelException("You are not high enough level to equip this item");
            }
            return false;
        }
    }
}
