﻿using Assignment1_RPG.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPG
{
    public class Mage : Character
    {

        public Mage()
        {
            Strength = 1;
            Dexterity = 1;
            Intelligence = 8;
            PrimaryAttribute = Intelligence;
        }
        public override void Stats(int lvl)
        {
            lvl = lvl - 1;
            Strength = Strength + (lvl * 1);
            Dexterity = Dexterity + (lvl * 1);
            Intelligence = Intelligence + (lvl * 5);
            totalPrimaryAttribute();
            PrimaryAttribute = Intelligence;
        }

        public override bool ItemCheck(Item item, int Level)
        {
            string[] allowedItems = new string[] { "Cloth", "Staff", "Wand" };
            if (item.ItemLevel <= Level)
            {
                foreach (string allowedItem in allowedItems)
                {
                    if (allowedItem == item.ItemType)
                        return true;
                };
            }
            if (item.ItemLevel > Level)
            {
                throw new InvalidItemLevelException("You are not high enough level to equip this item");
            }
            return false;
        }
    }
}